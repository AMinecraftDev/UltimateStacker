package com.songoda.ultimatestacker.events;

import com.songoda.ultimatestacker.entity.EntityStack;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-Feb-19
 */
public class StackDeathDropEvent extends EntityEvent implements Cancellable {

    private static HandlerList handlerList = new HandlerList();

    private final EntityStack entityStack;
    private final List<ItemStack> drops;
    private final int amountInStack;

    private boolean cancelled = false;

    public StackDeathDropEvent(Entity what, int amountInStack, EntityStack entityStack, List<ItemStack> drops) {
        super(what);

        this.drops = drops;
        this.entityStack = entityStack;
        this.amountInStack = amountInStack;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public int getAmountInStack() {
        return this.amountInStack;
    }

    public EntityStack getEntityStack() {
        return this.entityStack;
    }

    public List<ItemStack> getDrops() {
        return this.drops;
    }

    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
