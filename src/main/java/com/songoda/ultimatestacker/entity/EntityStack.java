package com.songoda.ultimatestacker.entity;

import com.songoda.ultimatestacker.UltimateStacker;
import com.songoda.ultimatestacker.utils.Methods;
import org.bukkit.entity.Entity;

public class EntityStack {

    private Entity entity;
    private int amount;

    public EntityStack(Entity entity, int amount) {
        this.entity = entity;
        this.setAmount(amount);
    }

    public void updateStack() {
        entity.setCustomNameVisible(true);
        entity.setCustomName(Methods.compileEntityName(entity, this.amount));
    }

    public Entity getEntity() {
        return entity;
    }

    protected void setEntity(Entity entity) {
        this.entity = entity;
    }

    public void addAmount(int amount) {
        this.amount += amount;

        updateStack();
    }

    public void removeAmount(int amount) {
        this.amount -= amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        if (amount == 1) {
            removeStackOnSingleAmount();
            this.amount = 1;
            return;
        }

        this.amount = amount;

        if (amount != 0) updateStack();
    }

    public void removeStackOnSingleAmount() {
        UltimateStacker.getInstance().getEntityStackManager().removeStack(this.entity);

        this.entity.setCustomName(null);
        this.entity.setCustomNameVisible(false);
    }

    @Override
    public String toString() {
        return "EntityStack:{"
                + "Entity:\"" + entity.getUniqueId().toString() + "\","
                + "Amount:\"" + amount + "\","
                + "}";
    }
}
